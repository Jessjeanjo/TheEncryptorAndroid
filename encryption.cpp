//
//  encryption.cpp
//
//
//  Created by Jessica Joseph on 11/03/15.
//
//

#include <iostream>
#include <string>

using namespace std;


void swap( char & first, char & second){
    
    char temp = first;
    first = second;
    second = temp;
    
}

void encrypt(int charCount, int shuffleCount, string & message){
    
    cout << "encrypting" << endl;
    
    for(int i = 0; i < shuffleCount; i++){
        for (int j = 0; j < message.length(); j++){
            if ( (j+charCount) > message.length())
                message.append(" ");
            swap(message[j], message[j+charCount] );
            
        }
    }
}



int main(){
    string determine, message;
    int charCount, shuffleCount;
    
    cout << "Would you like to encrypt or decode? Type encrypt or decode"<< endl;
    cin >> determine;
    
    if (determine == "encrypt"){
        
        
        cout << "What would you like to encrypt?" << endl;
        
        cin.ignore();
        getline(cin, message);
        
        cout << "I got your message:\n" << message << endl;
        
        
        
        cout << "The digits please, include a space"<< endl;
        cin >> charCount >> shuffleCount;
        
        encrypt(charCount, shuffleCount, message);
        cout << "Your new messgae, my friend, is down below. Enjoy!\n\n" << message << endl;
        
    } else if (determine == "decode"){
       // call decode function
    } else 
        cout << "Sorry that is not a valid answer, please try again!"<<endl; 
    
}
